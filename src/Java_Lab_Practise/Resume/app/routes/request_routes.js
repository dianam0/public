bodyParser = require('body-parser').json();

module.exports = function (app) {
        app.post('/request', bodyParser,(request, response) => {
        const {Client} = require('pg');

        const  client = new Client({user : 'postgres', port: '5432', host: 'localhost', database: 'resume', password: 'qwerty008'});
        client.connect();
        let  query = 'Insert into resume.public.request(name, num, email, comment) values ($1, $2, $3, $4);'
        client.query(query, [`${request.body.name}`, `${request.body.num}`, `${request.body.email}`, `${request.body.comment}`], (err) => {
            if (err != null) {
                response.send("Try again");
            }
            else {
                response.send("Thank you!");
            }
            client.end();
        });
    });
}


