const bodyParser = require('body-parser');
module.exports = function (app) {
    app.get('/user', (request, response) => {
        const result = [{
            "name": "Diana",
            "surname": "Monzhosova",
            "age": "19"
        }];
        response.setHeader("Content-Type", "application/json");
        response.send(JSON.stringify(result));
    });
    app.post('/user', (request, response) => {
        const result = [{
            "name": "Diana",
            "surname": "Monzhosova",
            "age": "19"
        }];
        response.setHeader("Content-Type", "application/json");
        response.send(JSON.stringify(result));
    });
};