package Java_Lab_Practise.Database.models;

import java.util.Objects;
import java.util.StringJoiner;

/**
 * 10.07.2020
 * 01. Database
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Mentor {
    private Long id;
    private String firstName;
    private String lastName;
    private Student student;

    public Mentor(long id, String firstName, String lastName, Student student) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.student = student;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, firstName, lastName, student);
    }

    @Override
    public String toString() {
        String printFN = firstName;
        String printLN = lastName;

        if (printFN != null) {
            printFN = printFN.trim();
        }

        if (printLN != null) {
            printLN = printLN.trim();
        }

        return new StringJoiner(", ", Mentor.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("firstName='" + printFN + "'")
                .add("lastName='" + printLN + "'")
                .toString();
    }
}
