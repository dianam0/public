package Java_Lab_Practise.Database;

import Java_Lab_Practise.Database.models.Student;
import Java_Lab_Practise.Database.repositories.StudentsRepository;
import Java_Lab_Practise.Database.repositories.StudentsRepositoryJdbcImpl;

import java.sql.*;

public class Main {

    private static final String URL = "jdbc:postgresql://localhost:5432/java_lab_pract";
    private static final String USER = "postgres";
    private static final String PASSWORD = "qwerty008";


    public static void main(String[] args) throws SQLException {
        Connection connection = DriverManager.getConnection(URL, USER, PASSWORD);
        StudentsRepository studentsRepository = new StudentsRepositoryJdbcImpl(connection);
        System.out.println(studentsRepository.findById((long) 8));

        studentsRepository.update(new Student((long) 12, "Петя", null, 21, 908));

        Student student = new Student(null, "Диана", "Монжосова", 19, 905);
        studentsRepository.save(student);
        System.out.println(student.getId());

        System.out.println(studentsRepository.findAllByAge(19));

        System.out.println(studentsRepository.findAll());
        connection.close();
    }
}
