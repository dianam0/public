package Java_Lab_Practise.Database.repositories;

import Java_Lab_Practise.Database.models.Mentor;
import Java_Lab_Practise.Database.models.Student;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

public class StudentsRepositoryJdbcImpl implements StudentsRepository {

    //language=SQL
    private static final String SQL_SELECT_BY_ID = "select * from student where id = ";
    private static final String SQL_SELECT_BY_AGE = "select * from student where age = ";
    private final Connection connection;

    public StudentsRepositoryJdbcImpl(Connection connection) {
        this.connection = connection;
    }

    @Override
    public List<Student> findAllByAge(int age) {
        Statement statement = null;
        ResultSet result = null;
        List<Student> list = new LinkedList<>();

        try {
            statement = connection.createStatement();

            result = statement.executeQuery(SQL_SELECT_BY_AGE + age);
            while (result.next()) {
                list.add(new Student(
                        result.getLong("id"),
                        result.getString("first_name"),
                        result.getString("last_name"),
                        result.getInt("age"),
                        result.getInt("group_number")));
            }
            return list;

        }
        catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
        finally {
            if (result != null) {
                try {
                    result.close();
                }
                catch (SQLException e) {
                    throw new IllegalArgumentException(e);
                }
            }
            if (result !=  null) {
                try {
                    result.close();
                }
                catch (SQLException e) {
                    throw new IllegalArgumentException(e);
                }
            }
        }
    }

    // Необходимо вытащить список всех студентов, при этом у каждого студента должен быть проставлен список менторов
    // у менторов в свою очередь ничего проставлять (кроме имени, фамилии, id не надо)
    // student1(id, firstName, ..., mentors = [{id, firstName, lastName, null}, {}, ), student2, student3
    // все сделать одним запросом
    @Override
    public List<Student> findAll() {
        Statement statement = null;
        ResultSet result = null;
        List<Student> list = new LinkedList<>();

        try {
            statement = connection.createStatement();
            result = statement.executeQuery("select * from student full outer join mentor on student.id = mentor.student_id");
            while (result.next()) {
                Student student = null;
                ListIterator<Student> iterator = list.listIterator();

                while (iterator.hasNext()) {
                    Student cursor = iterator.next();

                    if (cursor.getId() == result.getInt("id")) {
                        student = cursor;
                        break;
                    }
                }

                Mentor mentor = new Mentor(result.getLong(6),result.getString(7), result.getString(8), student);
                if (student == null) {
                    student = new Student(
                            result.getLong("id"),
                            result.getString("first_name"),
                            result.getString("last_name"),
                            result.getInt("age"),
                            result.getInt("group_number")
                    );

                    list.add(student);
                }

                student.setMentors(mentor);
            }

            return list;
        }
        catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
        finally {
            if (result != null) {
                try {
                    result.close();
                } catch (SQLException e) {
                    // ignore
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    // ignore
                }
            }
        }
    }

    @Override
    public Student findById(Long id) {
        Statement statement = null;
        ResultSet result = null;

        try {
            statement = connection.createStatement();
            result = statement.executeQuery(SQL_SELECT_BY_ID + id);
            if (result.next()) {
                return new Student(
                        result.getLong("id"),
                        result.getString("first_name"),
                        result.getString("last_name"),
                        result.getInt("age"),
                        result.getInt("group_number")
                );
            } else return null;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (result != null) {
                try {
                    result.close();
                } catch (SQLException e) {
                    // ignore
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    // ignore
                }
            }
        }
    }

    // просто вызывается insert для сущности
    // student = Student(null, 'Марсель', 'Сидиков', 26, 915)
    // studentsRepository.save(student);
    // // student = Student(3, 'Марсель', 'Сидиков', 26, 915)
    @Override
    public void save(Student entity) {
        PreparedStatement statement = null;
        ResultSet result = null;

        try {
            final String SQL = "insert into student (first_name, last_name, age, group_number) values ('"
                    + entity.getFirstName() + "', '" + entity.getLastName() + "' , " + entity.getAge() + " , "
                    + entity.getGroupNumber() + ")";

            statement = connection.prepareStatement(SQL, Statement.RETURN_GENERATED_KEYS);
            int affectedRows = statement.executeUpdate();

            if (affectedRows > 0) {
                try {
                    result = statement.getGeneratedKeys();
                    if (result.next()) {
                        entity.setId(result.getLong(1));
                    }
                }
                catch (SQLException e) {
                    throw new IllegalArgumentException(e);
                }
            }
        }
        catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
        finally {
            if (result != null) {
                try {
                    result.close();
                } catch (SQLException e) {
                    // ignore
                }
            }

            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    // ignore
                }
            }
        }
    }

    // для сущности, у которой задан id выполнить обновление всех полей

    // student = Student(3, 'Марсель', 'Сидиков', 26, 915)
    // student.setFirstName("Игорь")
    // student.setLastName(null);
    // studentsRepository.update(student);
    // (3, 'Игорь', null, 26, 915)

    @Override
    public void update(Student entity) {
        final String WHERE_ID = " where id = " + entity.getId() + ";";
        Statement statement = null;

        try {
            statement = connection.createStatement();
            statement.executeUpdate("update student set first_name = " + "'" + entity.getFirstName() + "'" + WHERE_ID +
                                        "update student set last_name = " + "'" + entity.getLastName() + "'" + WHERE_ID +
                                        "update student set age = " + entity.getAge() + WHERE_ID +
                                        "update student set group_number = " + entity.getGroupNumber() + WHERE_ID);
        }
        catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
        finally {
            if (statement != null) {
                try {
                    statement.close();
                }
                catch (SQLException e) {
                    //ignore
                }
            }
        }
    }
}
