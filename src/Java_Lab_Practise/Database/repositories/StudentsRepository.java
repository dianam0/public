package Java_Lab_Practise.Database.repositories;

import Java_Lab_Practise.Database.models.Student;

import java.util.List;

public interface StudentsRepository extends CrudRepository<Student> {
    List<Student> findAllByAge(int age);
}
